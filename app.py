import json
import time
import gspread
from collections import OrderedDict
from oauth2client.service_account import ServiceAccountCredentials

class Sheet:

    # column headers to their position.
    COMPANY = 1 
    APPLIED = 2 
    POSITION = 3 
    DESCRIPTION = 4 
    STATUS = 5 
    MULTIPLE = 6
    FIRSTDATAROW = 3

    def __init__(self, credential_path, title):
        self.sheet = Sheet.get_sheet(credential_path, title)
        self.title = title
    
    def get_height(self, start=1, company=None):
        '''
        returns the range of rows
        '''
        height = start 
        row = self.sheet.cell(height, 1);

        while row.value:
            if row.value == company:
                return height 
            height += 1
            row = self.sheet.cell(height, 1)

        # company is defined (was given, ie not None), but was not 
        # found in the while loop
        if company:
            raise KeyError('company %s is not in the self.sheet %s' % (
                company,
                self.sheet.title
                )
            )

        return height


    def add_job(self, company, position, description, status='P', row=-1):

        # bool: the company was already added and another position 
        # is being applied to
        has_multiple = False

        try: 
            row = row if row > 0 else self.get_height(company=company) 
            has_multiple = True
        except KeyError:
            row = row if row > 0 else self.get_height()

        today = time.strftime("%m/%d/%Y")
        
        # get the old data
        if has_multiple:
            new_data = {"jobs": []} 
            multiple_data = self.sheet.cell(row, Sheet.MULTIPLE).value

            if multiple_data:
                new_data = json.loads(multiple_data)

            ordered_data = OrderedDict()
            new_data["jobs"].append(
                OrderedDict([
                    ('position', position),
                    ('description', description),
                    ('date', today),
                    ('status', status)
                ])
                )
            self.sheet.update_cell(row, Sheet.MULTIPLE, json.dumps(new_data))
     

        self.sheet.update_cell(row, Sheet.COMPANY, company) 
        self.sheet.update_cell(row, Sheet.APPLIED, today) 
        self.sheet.update_cell(row, Sheet.POSITION, position) 
        self.sheet.update_cell(row, Sheet.DESCRIPTION, description) 
        self.sheet.update_cell(row, Sheet.STATUS, status)

        return True

    def update_status(self, job, status):
        try:
            row = get_height(company=company)
        except KeyError:
            return False

        self.sheet.update_cell(row, Sheet.STATUS, status)
        return True

    @staticmethod
    def get_sheet(credential_path, title):
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
                credential_path, scope)
        gc = gspread.authorize(credentials)

        jobsheet = gc.open(title).sheet1

        return jobsheet

