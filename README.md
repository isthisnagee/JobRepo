# JobRepo

This is a repo that let's you add jobs you have applied to 
or will apply to on google sheets

Follow the instructions on [gspread's README](https://github.com/burnash/gspread)
about getting the sheets API credentials. Save the json file as credentials.json under
this directory.

Don't forget to share the sheet with the email given to you by the
sheets api.

###Usage

1. (consider using virtualenv)
2. run `pip install -r requirements.txt`
3. run `python cli.py`

###Future

* Ability to add only a company name
* Sort jobs by name (1 name / sheet?)
* Faster (seems like gspread is really slow)
